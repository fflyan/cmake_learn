#include <stdio.h>

#include "cJSON.h"

#define tmp_size (20 * 1024)

int main(int argc, char *argv[])
{
    unsigned int inum = 0;
    char tmp[tmp_size] = {0};
    cJSON *json = NULL;
    cJSON *array = NULL;
    cJSON *subparm = NULL;

    json = cJSON_CreateObject();
    cJSON_AddNumberToObject(json, "argc", argc);
    array = cJSON_CreateArray();
    cJSON_AddItemToObject(json, "param", array);
    
    for (inum = 0; inum < argc; inum++)
    {
        subparm = cJSON_CreateObject();
        cJSON_AddNumberToObject(subparm, "count", inum);
        cJSON_AddStringToObject(subparm, "arg", argv[inum]);
        cJSON_AddItemToArray(array, subparm);
    }

    printf("%s\n", cJSON_Print(json));

    // printf("tmp=%s\n", tmp);
    // printf("inum=%d\r\n", inum);
    return 0;
}
