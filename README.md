## 学习cmake及ninja留存的记录

1. 前置软件安装：cmake，ninja

2. 编译：
```sh
mkdir build
cd build
cmake ../src/ -Bninja -GNinja
export NINJA_STATUS="[%p/%f/%t %e] ";ninja -d keepdepfile -d stats -d explain -C ninja/
```
生成可执行文件：*./build/output/bin/main*
生成库文件：*./build/output/lib/libcJSON.so*

3. 运行：
```sh
./output/bin/main test
```
